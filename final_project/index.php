<!doctype html>
<!-- 
    File: index.php
    Author: Stephen Hoadley
    Date: 12 - 13 - 2018
    Purpose: What the user sees in the app, handle all of the visual aspects of the app
-->
<html lang="en">
    <!-- Based on: https://getbootstrap.com/docs/4.1/examples/sign-in/ -->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="A program that accepts edge and node files and performs data analysis on them.">
        <meta name="author" content="Stephen Hoadley">

        <title>Edge+Node Data</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <script>
            var exportEdgeFile = function(event){
                $.ajax({
                    url: 'edge+node.php',
                    method: 'post',
                    data: {
                        action: 'export_edges',
                        file_id: event.target.file
                    },
                    success: (response)=>{
                        console.log(response);
                        var data = JSON.parse(response);
                        if(data.success){
                            var id = data.id;
                            
                        } else {
                            alert(data.error);
                        }
                    },
                    error: (xhr, status, error)=>{
                        alert(errors);
                    }
                });
            };

            var importNodes = function(event){
                $.ajax({
                    url: 'edge+node.php',
                    method: 'post',
                    data: {
                        action: 'export_nodes',
                        file_id: event.target.file
                    },
                    success: (response)=>{
                        console.log(response);
                        var data = JSON.parse(response);
                        if(data.success){
                            var id = data.id;
                            
                        } else {
                            alert(data.error);
                        }
                    },
                    error: (xhr, status, error)=>{
                        alert(errors);
                    }
                });
            }

            $(document).ready(function(){
                $(document).on('change', '#exportEdgeFile', exportEdgeFile);
                $(document).on('change', '#importNodes', exportNodes);
            });

        </script>
    </head>

    <body class="text-left" id="todo-page">
        <input type="file" id="exportEdgeFile" name="file"/>
        <input type="file" id="importNodes" name="file"/>
        <table>
            <?php foreach($edgeArray as $edge){ ?>
                <tr>
                    <td><?php echo $edge; ?></td>
                </tr>
            <?php } ?>
            <?php foreach($nodeArray as $node){ ?>
                <tr>
                    <td><?php echo $node; ?> <button id="nodeMetrics">Extra Node Stuff</button></td>
                    <p id="nodeStuff">In-Degree: </p>
                    <p id="nodeStuff">Out-Degree: </p>
                    <p id="nodeStuff">Degree (In and Out): </p>
                    <p id="nodeStuff">Betweeness Centrality: </p>
                    <p id="nodeStuff">Closeness Centrality: </p>
                </tr>
            <?php } ?>
        </table>
        <h4>Total Edges: <?php echo $total_edges; ?></h4>
        <h4>Total Nodes: <?php echo $total_nodes; ?></h4>
        <h4>Unique Edges: <?php echo $unique_edges; ?></h4>
        <h4>Maximum Geodesic Distance: <?php echo $maximum_geodesic_distance; ?></h4>
        <h4>Average Geodesic Distance: <?php echo $average_geodesic_distance; ?></h4>
        <h4>Density: <?php echo $density; ?></h4>
        <h4>Number of Conected Components: <?php echo $number_of_connected_components; ?></h4>
        <h4>Average Number of Nodes and Edges Across Connected Components: <?php echo $average_num_nodes_edges_components; ?></h4>
        <h4>Average In-Degree: <?php echo $average_in_degree; ?></h4>
        <h4>Average Out-Degree: <?php echo $average_out_degree; ?></h4>
        <h4>Average Degree (in + out): <?php echo $average_degree_in_out; ?></h4>
        <button>Export Data</button>
    </body>
</html>