<?php
// File:        dataBase.php
// Author:      Stephen Hoadley
// Date:        12 - 13 - 2018
// Purpose:     Potential to store the arrayList from the file passed in
	function setupDB(){
	    global $dbh;
	    try {
	        $dbh->exec("create table if not exists edges(".
	            "id integer primary key autoincrement,".
	            "start_node text NOT NULL,".
	            "end_node text NOT NULL,".
	            "directed boolean".
	            "label text,".
	            "color text,".
	            "size integer)");

	        // Check for errors.
	        $error = $dbh->errorInfo();
	        if($error[0] !== '00000' && $error[0] !== '01000') {
	            die("There was an error setting up the edges table: ". $error[2]);
	        }

	        $dbh->exec("create table if not exists nodes(".
	            "id integer primary key autoincrement,".
	            "node_name text NOT NULL,".
	            "label text,".
	            "color text,".
	            "size integer)");

	        // Check for errors.
	        $error = $dbh->errorInfo();
	        if($error[0] !== '00000' && $error[0] !== '01000') {
	            die("There was an error setting up the nodes table: ". $error[2]);
	        }
	    } catch(PDOException $e) {
	        die("There was an error setting up the database: ". $e->getMessage());
	    }
	}

	function getEdgeList(){
	    global $dbh;
	    connectToDB();

	    try {
	        $statement = $dbh->prepare(
	            "select id,start_node,end_node,directed,label,color,size from todo_items  ".
	            );
	        $success = $statement->execute(array(
	            ":user_id" => $userId));
	            
	        if(!$success){
	            die("There was an error reading from the database: ". 
	                $dbh->errorInfo()[2]);
	        } else {
	            return $statement->fetchAll(PDO::FETCH_ASSOC);
	        }
	    } catch(PDOException $e){
	        die("There was an error reading from the database: ". 
	            $e->getMessage());
	    }
	}
?>