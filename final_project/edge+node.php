<?php
// File:        edge+node.php
// Author:      Stephen Hoadley
// Date:        12 - 13 - 2018
// Purpose:     Handles all the calculations and data for index.php

session_start()
// Code retrieved from https://stackoverflow.com/questions/13811385/pass-array-from-one-page-to-another
$_SESSION['edge-array'] = $edgeArray;
redirect_to("../index.php");

require_once("dataBase.php");

$ACTION_MAP = array(
	"export_edges" => "exportEdges",
	"import_nodes" => "importNodes"
);

global $edgeArray;
global $weight;
global $start_node;
global $end_node;

global $total_nodes;
global $total_edges = 0;
global $unique_edges;
global $maximum_geodesic_distance;
global $average_geodesic_distance;
global $density;
global $number_connected_components;
global $average_num_nodes_edges_components;
global $average_in_degree;
global $average_out_degree;
global $average_degree_in_out;

$in_degree = 0
$out_degree = 1
$degree_in_and_out = 2
$betweeness_centrality = 3
$closeness_centrality = 4

function exportEdges($data){
	//Code retrieved from https://stackoverflow.com/questions/4801895/csv-to-associative-array
	$edgeArray = $edges = array(); $i = 0;
	$handle = @fopen($data"file_id", "r");
	if ($handle) {
	    while (($row = fgetcsv($handle, 4096)) !== false) {
	        if (empty($edges)) {
	            $edges = $row;
	            continue;
	        }
	        foreach ($row as $k=>$value) {
	            $edgeArray[$i][$edges[$k]] = $value;
	            $total_edges = $total_edges + 1;
	        }
	        $i++;
	    }
	    if (!feof($handle)) {
	        echo "Error: unexpected fgets() fail\n";
	    }
	    fclose($handle);
	}
	computeMetrics();
}

function computeMetrics(){
	// Lines up the headers in the file with the arrayList
	foreach ($row as $k) {
		if ($edgeArray[0][$k] == "Weight") {
		    $weight = k
		}
		if ($edgeArray[0][$k] == "Vertex 1") {
			$start_node = k
		}
		if ($edgeArray[0][$k] == "Vertex 2") {
			$end_node = k
		}		
	}
	// From here on should compute the rest of the metrics from edge data
}

function importNodes($data){
	//Code retrieved from https://stackoverflow.com/questions/4801895/csv-to-associative-array
	global $nodeArray = $nodes = array(); $i = 0;
	$handle = @fopen($data"file_id", "r");
	if ($handle) {
	    while (($row = fgetcsv($handle, 4096)) !== false) {
	        if (empty($nodes)) {
	            $nodes = $row;
	            continue;
	        }
	        foreach ($row as $k=>$value) {
	            $nodeArray[$i][$nodes[$k]] = $value;
	        }
	        $i++;
	    }
	    if (!feof($handle)) {
	        echo "Error: unexpected fgets() fail\n";
	    }
	    fclose($handle);
	}
	computeNodeMetrics();
}

function computeNodeMetrics(){
	global $nodeMetricArray = array();
	foreach ($nodeArray as $nodes){
		foreach ($nodeMetricArray as $node => $value) {
			// computes the metrics for each node in the nodeArray
		}
	}
}

?>